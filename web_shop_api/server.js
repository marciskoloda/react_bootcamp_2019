const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const User = require("./app/models/user");
const routes = require("./app/routes/routes");

mongoose.connect("mongodb://localhost:27017/computerStore", {
  useNewUrlParser: true
});
mongoose.set("useCreateIndex", true);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const port = process.env.PORT || 8080;

app.use("/api", routes);
// =============================================================================
app.listen(port);
console.log("Magic happens on port " + port);
