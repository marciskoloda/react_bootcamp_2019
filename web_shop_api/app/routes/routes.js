const express = require("express");
const router = express.Router();
const User = require("../models/user");
const Product = require("../models/product");

router.use(function(req, res, next) {
  //

  next();
});

router.get("/", function(req, res) {
  const response = {
    title: "This is sample api server",
    msg:
      "This server is test server for React bootcamp. All data will be wiped after few hours. Data is available using route /api "
  };
  res.json(response);
});

// ------------ USER Routes ----------------- //
router
  .route("/users")
  .post((req, res) => {
    const { name, email, username, password, passwordConf } = req.body;

    if (name && email && username && password && passwordConf) {
      const user = new User();

      user.name = req.body.name;
      user.email = req.body.email;
      user.username = req.body.username;
      user.password = req.body.password;
      user.passwordConf = req.body.passwordConf;

      user.save(err => {
        if (err) {
          res.send(err);
        }
        res.json({ message: "User created" });
      });
    } else {
      res.json({ message: "Missing data" });
    }
  })
  .get((req, res) => {
    User.find((err, users) => {
      if (err) res.send(err);

      res.json(users);
    });
  });

router
  .route("/users/:user_id")
  .get((req, res) => {
    User.findById(req.params.user_id, (err, user) => {
      if (err) {
        res.json(err);
      }

      res.json(user);
    });
  })
  .put((req, res) => {
    User.findById(req.params.user_id, (err, user) => {
      if (err) {
        res.json(err);
      }

      user.name = req.body.name;

      user.save(err => {
        if (err) {
          res.json(err);
        }

        res.json({ message: `User ${user.name} updated` });
      });
    });
  });

// ------------ USER Routes END ----------------- //

// ------------ PRODUCT Routes ----------------- //

router
  .route("/products")
  .post((req, res) => {
    const {
      name,
      price,
      available,
      availableCount,
      description,
      category_id
    } = req.body;

    if (name) {
      const product = new Product();

      product.name = name;
      product.price = price;
      product.available = available;
      product.availableCount = availableCount;
      product.description = description;
      product.date_added = new Date();
      product.category_id = category_id;

      product.save(err => {
        if (err) {
          res.send(err);
        }
        res.json(product);
      });
    } else {
      res.json({ message: "Missing data" });
    }
  })
  .get((req, res) => {
    Product.find((err, products) => {
      if (err) res.send(err);

      res.json(products);
    });
  });

router
  .route("/products/:product_id")
  .get((req, res) => {
    Product.findById(req.params.product_id, (err, product) => {
      if (err) {
        res.json(err);
      }

      res.json(product);
    });
  })
  .put((req, res) => {
    Product.findById(req.params.product_id, (err, product) => {
      if (err) {
        res.json(err);
      }

      product.name = req.body.name;

      product.save(err => {
        if (err) {
          res.json(err);
        }

        res.json(product);
      });
    });
  });

router
  .route("/products/:product_id/available")
  .get((req, res) => {
    Product.findById(req.params.product_id, (err, product) => {
      if (err) {
        res.json(err);
      }

      res.json({ available: product.availableCount });
    });
  })
  .put((req, res) => {
    Product.findById(req.params.product_id, (err, product) => {
      console.log(product, req);

      // if (err) {
      //   res.json(err);
      // }

      // const { available } = req.body;
      // if (available) {
      //   product.availableCount = available;

      //   product.save(err => {
      //     if (err) {
      //       res.json(err);
      //     }

      //     res.json(product);
      //   });
      // }

      // res.json({ available: product.availableCount });
    });
  });

module.exports = router;
