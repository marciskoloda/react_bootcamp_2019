const mongoose = require("mongoose");

const ProductSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  price: {
    type: String,
    required: true
  },
  available: Boolean,
  availableCount: Number,
  description: {
    manufacturer: String,
    specs: Object
  },
  date_added: Date,
  category_id: String
});

const Product = mongoose.model("Product", ProductSchema);
module.exports = Product;
