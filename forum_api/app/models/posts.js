const mongoose = require("mongoose");
//Define a schema
const Schema = mongoose.Schema;
const PostsSchema = new Schema({
  title: {
    type: String,
    trim: true,
    required: true
  },
  body: {
    type: String,
    trim: true,
    required: true
  },
  userId: {
    type: String,
    required: true
  },
  createdAt: {
    type: Schema.ObjectId,
    trim: true,
    required: true
  }
});
module.exports = mongoose.model("Posts", PostsSchema);
