const postsModel = require("../models/posts");
module.exports = {
  getById: function(req, res, next) {
    console.log(req.body);
    postsModel.findById(req.params.postsId, function(err, post) {
      if (err) {
        next(err);
      } else {
        res.json({
          status: "success",
          message: "post found",
          data: { post }
        });
      }
    });
  },
  getAll: function(req, res, next) {
    postsModel.find({}, function(err, posts) {
      if (err) {
        next(err);
      } else {
        res.json({
          status: "success",
          message: "posts found",
          data: { posts }
        });
      }
    });
  },

  create: function(req, res, next) {
    postsModel.create(
      {
        title: req.body.title,
        body: req.body.body,
        userId: req.body.userId,
        createdAt: new Date()
      },
      function(err, result) {
        if (err) next(err);
        else
          res.json({
            status: "success",
            message: "Post added",
            data: { result }
          });
      }
    );
  }
};
