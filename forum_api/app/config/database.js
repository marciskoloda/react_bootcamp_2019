//Set up mongoose connection
const mongoose = require("mongoose");
mongoose.connect(
  "mongodb+srv://students:react_apmacibas_2019@cluster0-yxtsk.mongodb.net/test?retryWrites=true&w=majority",
  {
    useNewUrlParser: true
  }
);
mongoose.set("useCreateIndex", true);
mongoose.Promise = global.Promise;
module.exports = mongoose;
