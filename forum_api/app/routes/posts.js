const express = require("express");
const router = express.Router();
const postsController = require("../controllers/posts");
router.get("/", postsController.getAll);
router.post("/", postsController.create);
router.get("/:postsId", postsController.getById);
module.exports = router;
