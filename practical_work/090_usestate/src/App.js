import React, { useState, useEffect } from "react";
import "./App.css";
import "antd/dist/antd.css";
import { Button, Icon, Typography } from "antd";

function App() {
  const [count, setCount] = useState(100);

  useEffect(() => {
    document.title = count;
  });

  return (
    <div className="App" style={{ marginTop: "25%" }}>
      <div>
        <Typography.Title>{count}</Typography.Title>
      </div>
      <div>
        <Button.Group size={"large"}>
          <Button onClick={() => setCount(count - 1)}>
            <Icon type="minus-square" />
            Samazināt
          </Button>
          <Button onClick={() => setCount(count + 1)}>
            Palielināt
            <Icon type="plus-square" />
          </Button>
        </Button.Group>
      </div>
    </div>
  );
}

export default App;
