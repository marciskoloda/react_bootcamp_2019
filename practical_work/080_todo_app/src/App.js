import React, { Component } from "react";
import "./App.css";
import "antd/dist/antd.css";
import "./index.css";
import { Layout, Typography } from "antd";
import MainInput from "./main-input";
import TodoItems from "./todo-items";

const { Title } = Typography;
const { Header, Content, Footer } = Layout;

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      todos: []
    };
  }

  handleTodoItemAdd = item => {
    const todos = this.state.todos;

    const todoItem = {
      ...item,
      done: false
    };
    todos.push(todoItem);

    this.setState({ todos: todos });
  };

  handleTodoItemRemove = todoIndex => {
    const todos = this.state.todos;
    todos.splice(todoIndex, 1);

    this.setState({ todos: todos });
  };

  handleTodoItemSetDone = todoIndex => {
    const todos = this.state.todos;
    todos[todoIndex].done = true;

    this.setState({ todos });
  };

  render() {
    return (
      <Layout className="layout">
        <Header>
          <Title style={{ color: "white", textAlign: "center" }}>
            Todo aplikācija - React apmācības
          </Title>
        </Header>
        <Content style={{ padding: "50px" }}>
          <div style={{ background: "#fff", padding: 24, minHeight: 280 }}>
            <MainInput
              todos={this.state.todos}
              setTodo={this.handleTodoItemAdd}
            />
            <TodoItems
              todos={this.state.todos}
              onRemove={this.handleTodoItemRemove}
              onComplete={this.handleTodoItemSetDone}
            />
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          &copy; Mārcis Koloda | Design: Ant Design
        </Footer>
      </Layout>
    );
  }
}

export default App;
