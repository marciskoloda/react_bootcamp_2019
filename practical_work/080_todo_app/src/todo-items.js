import React, { Component } from "react";
import { List, Card, Button, Tag, Icon } from "antd";

class TodoItems extends Component {
  getActions = (todo, index) => {
    const { onRemove, onComplete } = this.props;

    const actions = [];

    if (!todo.done) {
      actions.push(
        <Button type="primary" block onClick={() => onComplete(index)}>
          Pabeigt
        </Button>
      );
    } else {
      actions.push(
        <Button type="danger" block onClick={() => onRemove(index)}>
          Noņemt
        </Button>
      );
    }

    return actions;
  };

  render() {
    const { todos } = this.props;
    return (
      <List
        grid={{
          gutter: 16,
          xs: 1,
          sm: 2,
          md: 4,
          lg: 4,
          xl: 6,
          xxl: 3
        }}
        dataSource={todos}
        renderItem={(item, index) => (
          <List.Item>
            <Card
              title={
                <>
                  <Tag color={item.done ? "green" : "red"}>
                    {item.done ? "DONE" : "TODO"}
                  </Tag>
                  {item.title}
                </>
              }
              actions={this.getActions(item, index)}
            >
              {item.description !== "" ? item.description : "--"}
            </Card>
          </List.Item>
        )}
        locale={{
          emptyText: (
            <>
              <Icon type="smile" /> Viss izdarīts!{" "}
            </>
          )
        }}
      />
    );
  }
}

export default TodoItems;
