import React, { Component } from "react";
import { Input, Button, Alert } from "antd";

const { TextArea } = Input;

class MainInput extends Component {
  constructor(props) {
    super(props);

    this.state = {
      warn: false,
      todo: {
        title: "",
        description: ""
      }
    };
  }

  handleChange = event => {
    const { name, value } = event.target;

    this.setState({ warn: false, todo: { ...this.state.todo, [name]: value } });
  };

  handleKeyPress = event => {
    const { setTodo } = this.props;

    if (event.key === "Enter") {
      setTodo(this.state.todo);

      this.setState({ todo: { title: "", description: "" } });
    }
  };

  handleSave = () => {
    const { setTodo } = this.props;

    if (this.state.todo.title !== "") {
      setTodo(this.state.todo);
      this.setState({ todo: { title: "", description: "" } });
    } else {
      this.setState({ warn: true });
    }
  };

  render() {
    return (
      <div style={{ padding: "20px" }}>
        {this.state.warn && (
          <Alert
            message="Todo nosaukums nav ievadīts"
            type="warning"
            showIcon
            style={{ marginBottom: "20px" }}
          />
        )}
        <Input
          name="title"
          size="large"
          placeholder="Todo nosaukums"
          onKeyPress={this.handleKeyPress}
          onChange={this.handleChange}
          value={this.state.todo.title}
          style={{ marginBottom: "10px" }}
        />

        <TextArea
          name="description"
          placeholder="Todo apraksts..."
          autosize={{ minRows: 2, maxRows: 6 }}
          onChange={this.handleChange}
          value={this.state.todo.description}
          style={{ marginBottom: "10px" }}
        />

        <Button type="primary" onClick={this.handleSave}>
          Pievienot
        </Button>
      </div>
    );
  }
}

export default MainInput;
