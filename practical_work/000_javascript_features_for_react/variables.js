/**
 * [1] Atsķirība starp var un let
 */

// Javascript ES6 ( Ecmascript ) ir pieejami 3 mainīgo veidi
// [ 1 ] var
// [ 2 ] let
// [ 3 ] const

function variableTest() {
  for (let index = 0; index < 1; index++) {
    var functionVar = "var";

    console.log("blokā", { index, functionVar });
  }

  console.log("let ārpus bloka", { index }); // let mainīgais nav pieejams ārpus bloka, kurā tika izveidots, šis argriezīs kļūdu
  console.log("var ārpus bloka", { functionVar }); // var mainīgais vēlaizvien ir pieejams
}

// variableTest();

/**
 * [2] Javascript konstante
 */

function constantText() {
  const c = 299792458;
  console.log({ c });

  c = 1; // Šis nestrādās, jo `c` ir konstante, jeb read only mainīgais, kuru nav iespējams pārdeklerēt
}

// constantText();

/**
 * [3] Javascript objekti
 */
