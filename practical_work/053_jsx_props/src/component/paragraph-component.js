import React from "react";

export const ParagraphComponent = props => {
  return <p className="paragraph">{props.children}</p>;
};
