import React from "react";

// ES6 arrow funkcija, kā React komponente
const Paragraph = props => {
  return <p>{props.text}</p>;
};

function Welcome(props) {
  return (
    <React.Fragment>
      <h1>Sveiks, {props.name}</h1>
      <Paragraph text="Šī ir teksta komponente" />
      <Paragraph text="Šādas komponentes, komponentē var būt daudz" />
      <Paragraph text="Bet visām jābūt zem vienas komponentes vai jāatgriež kā masīvs" />
    </React.Fragment>
  );
}

export const Sample = () => {
  return <Welcome name="Mārcis" />;
};
