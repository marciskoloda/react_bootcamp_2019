import React, { Component } from "react";
import { Card, Icon, Avatar } from "antd";
import defaultCoverImage from "../resources/images/cover.jpg";
import defaultAvatarImage from "../resources/images/avatar.png";

const { Meta } = Card;

class UserInfoComponent extends Component {
  render() {
    const { name, surname, degree, avatar, cover, hoverable } = this.props;

    return (
      <div
        className="user"
        style={{ width: 300, margin: "auto", padding: "50px 0" }}
      >
        <Card
          style={{ width: 300 }}
          cover={
            <img
              alt={`${name} ${surname} cover`}
              src={cover || defaultCoverImage}
            />
          }
          actions={[
            <Icon type="setting" />,
            <Icon type="edit" />,
            <Icon type="ellipsis" />
          ]}
          hoverable={hoverable}
        >
          <Meta
            avatar={<Avatar src={avatar || defaultAvatarImage} />}
            title={`${name} ${surname}`}
            description={`Zin. grāds: ${degree}`}
          />
        </Card>
      </div>
    );
  }
}

export default UserInfoComponent;
