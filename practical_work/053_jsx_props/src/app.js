import React from "react";
import UserInfoComponent from "./component/user-info-component";
import "./app.css";
import avatar from "./resources/images/avatar.png";
import cover from "./resources/images/cover2.jpg";
import { ParagraphComponent } from "./component/paragraph-component";
import { Sample } from "./component/sample";

const App = () => {
  const alfreds = {
    name: "Alfrēds",
    surname: "Kāpostgalva",
    degree: "-",
    avatar: null,
    cover: cover
  };

  return (
    <div className="container">
      <UserInfoComponent
        name="Mārcis"
        surname={"Koloda"}
        degree="Mag.sc.comp"
        avatar={avatar}
        hoverable
      />

      <UserInfoComponent {...alfreds} />

      <ParagraphComponent>
        Šis ir random teksts, kas tiek uztverts kā paragrāfs
      </ParagraphComponent>

      <Sample />
    </div>
  );
};

export default App;
