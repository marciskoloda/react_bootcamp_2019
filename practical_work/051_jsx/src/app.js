import React from "react";
import { UserInfo } from "./components/user-info";

const App = () => {
  return (
    <div className="container">
      <UserInfo name="Mārcis" surname="Koloda" age={29} />
    </div>
  );
};

export default App;
