import React from "react";

export const UserInfo = props => {
  return (
    <div className="user-info">
      <h3>
        {props.name} {props.surname}
      </h3>
      <p>Vecums: {props.age}</p>
    </div>
  );
};
