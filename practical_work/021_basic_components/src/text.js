import React from "react";

const Text = props => {
  return React.createElement("p", {}, props.text);
};

export default Text;
