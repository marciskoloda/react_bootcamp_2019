import React from "react";
import ReactDOM from "React-dom";
import Title from "./title";
import Text from "./text";

const App = () => {
  return React.createElement("div", { className: "app" }, [
    React.createElement(Title, { key: 1, title: "Hello world" }),
    React.createElement(Text, {
      key: 2,
      text:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris mattis diam non ante pharetra, id aliquam odio varius. Mauris lorem orci, feugiat id sollicitudin id, dictum non sapien. Quisque volutpat mollis felis vitae dictum. Maecenas tristique imperdiet dolor, non porta tortor consectetur ac. In non ullamcorper erat, eu ornare dolor. Nam ut nisi ac sapien maximus blandit vel vitae elit. Fusce consectetur dui id commodo laoreet. Proin facilisis metus mattis, efficitur enim vitae, porta risus. Quisque vestibulum dapibus varius. Cras egestas faucibus lacus id pretium. Integer nec urna turpis. Proin eget tincidunt lorem, nec scelerisque neque."
    })
  ]);
};

ReactDOM.render(React.createElement(App, {}), document.getElementById("root"));
