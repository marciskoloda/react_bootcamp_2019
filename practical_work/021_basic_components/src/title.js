import React from "react";

const Title = props => {
  return React.createElement("h1", {}, props.title);
};

export default Title;
