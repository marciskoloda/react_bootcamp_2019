export class Teacher extends Person {
  constructor(name, surname, age, degree) {
    super(name, surname, age);
    this.degree = degree;
  }

  present() {
    console.log("prezentēt nodarbību");
  }
}
