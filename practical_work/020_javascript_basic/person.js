export class Person {
  constructor(name, surname, age) {
    this.name = name;
    this.surname = surname;
    this.age = age;
  }

  walk() {
    console.log("staigāt");
  }
}
