import {Person} from "./person";
import {Teacher } from "./teacher";


const marcis = new Person("Mārcis", "Koloda", 29);
const pasniedzejs = new Teacher(...marcis, "Mag.sc.comp.");
pasniedzejs.present();