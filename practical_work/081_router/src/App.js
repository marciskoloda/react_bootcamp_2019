import React from "react";
import "./App.css";
import { Router, Link } from "react-router-dom";
import createHistory from "history/createBrowserHistory";
import { Routes } from "./routes";
import { Layout, Menu, Icon } from "antd";
import "antd/dist/antd.css";

const history = createHistory();
const { Header, Content, Footer } = Layout;

function App() {
  return (
    <Layout className="layout">
      <Router history={history}>
        <Header>
          <div className="logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={["1"]}
            style={{ lineHeight: "64px" }}
          >
            <Menu.Item key="1">
              <Link to="/">
                <Icon type="home" />
              </Link>
            </Menu.Item>
            <Menu.Item key="2">
              <Link to="/cat">Kaķi</Link>
            </Menu.Item>
            <Menu.Item key="3">
              <Link to="/dog">Suņi</Link>
            </Menu.Item>
          </Menu>
        </Header>
        <Content style={{ padding: "0 50px" }}>
          <div style={{ background: "#fff", padding: 24, minHeight: 280 }}>
            <Routes />
          </div>
        </Content>
        <Footer style={{ textAlign: "center" }}>
          &copy; Mārcis Koloda | Design: Ant Design
        </Footer>
      </Router>
    </Layout>
  );
}

export default App;
