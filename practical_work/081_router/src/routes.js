import React from "react";
import { Route, Switch } from "react-router-dom";
import { Home } from "./home";
import { Dog } from "./dog";
import { Cat } from "./cat";
import { NotFound } from "./not-found";

const Routes = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/dog" component={Dog} />
        <Route path="/cat" component={Cat} />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
};

export { Routes };
