import React from "react";
import { UserInfo } from "./components/user-info";
import { Form } from "./components/form";

const App = () => {
  return (
    <div className="container">
      <UserInfo name="Mārcis" surname="Koloda" age={29} />
      <Form.Input type="text" placeHolder="Input lauks" />
      <Form.Checkbox />
    </div>
  );
};

export default App;
