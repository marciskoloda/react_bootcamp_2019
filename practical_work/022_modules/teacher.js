import React from "react";
import { Person } from "./person";

export class Teacher extends Person {
  constructor(name, surname, age, degree) {
    super(name, surname, age);
    this.degree = degree;
  }

  present() {
    console.log("prezentēt nodarbību");
  }
}
