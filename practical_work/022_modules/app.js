import React from "react";
import { Person } from "./person";
import { Teacher } from "./teacher";

export const App = () => {
  const marcis = new Person("Mārcis", "Koloda", 29);
  const pasniedzejs = new Teacher(
    marcis.name,
    marcis.surname,
    marcis.age,
    "Mag.sc.comp."
  );
  pasniedzejs.present();

  return React.createElement(
    "h1",
    {},
    `Sveiki, mans vārds ir ${marcis.name} ${marcis.surname} `
  );
};
